[![crates.io](https://img.shields.io/crates/v/napa.svg)](https://crates.io/crates/napa)
[![license](https://img.shields.io/crates/l/napa.svg)](https://www.mozilla.org/en-US/MPL/2.0/)
[![dependency status](https://deps.rs/repo/codeberg/jwgarber/napa/status.svg)](https://deps.rs/repo/codeberg/jwgarber/napa)
[![builds.sr.ht status](https://builds.sr.ht/~jwgarber/napa.svg)](https://builds.sr.ht/~jwgarber/napa?)

### Introduction

Napa is a simple command line password manager written in Rust. Napa should be considered beta-level: it should not be used to store any actual passwords, and the database format may change in the future. At the moment Napa is Linux-only and requires Wayland for clipboard support. Patches for other FOSS operating systems are welcome.

### Installation

Statically-linked binaries are published on the [releases](https://codeberg.org/jwgarber/napa/releases) page.[^1] Alternatively, if you have a Rust toolchain you can build from source using:

```
RUSTFLAGS='-C target-cpu=native' cargo install napa
```

(Native CPU makes Argon2 run faster.)

### Motivation

The most popular command line password manager today is probably [pass](https://www.passwordstore.org/), which I used happily for several years. However, pass uses GPG to encrypt its secrets, which requires the song-and-dance of maintaining a GPG key just to access your passwords. [GPG is also bad](https://latacora.micro.blog/2019/07/16/the-pgp-problem.html), and its aging cryptography means that pass is vulnerable to [database attacks](https://rot256.dev/post/pass/). Pass *also* gives arbitrary programs read/write access to your passwords once your GPG key is unlocked, which made me increasingly nervous in today's age of [typosquatting](https://www.zdnet.com/article/two-malicious-python-libraries-removed-from-pypi/). Thus was born Napa, a much simpler password manager with stronger cryptography for when you don't completely trust your local computer.

### Design Goals

In reaction to the [enormous complexity](https://github.com/lfit/itpol/blob/master/protecting-code-integrity.md) of GPG,
Napa was designed with the goal of "radical simplicity". I am happy to report that Napa has no configurable user settings. In particular,

- There is no user selection of cryptographic algorithms or parameters; a [single secure](https://www.imperialviolet.org/2016/05/16/agility.html) default is chosen, and if necessary that will be updated in the future with a version number bump.
- There is no user choice of passphrase for the database file. Humans have a terrible habit of reusing passwords, so the main passphrase is securely generated for you.

### Usage

A new database can be initialized using `napa init`. This will generate a six word passphrase from the [EFF long word list](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases).[^2] Remember this passphrase, since it won't be displayed again.

```
$ napa init my-passwords.napa
The new passphrase for this database file is: creme-customer-squad-persuader-wise-purveyor
Remember this passphrase! It will not be displayed again.
```

After initialization, the database file can be unlocked using `napa open`. This will cache the decryption key in memory and drop you into a REPL-like interface for managing the password entries.

```
$ napa open my-passwords.napa
Enter the passphrase:
> edit example-entry
# this will open the entry in a built-in editor
```

The name of each password entry can be arbitrary text, which allows you to group the passwords however you like. Password entries can also contain arbitrary text and have no pre-defined layout. The only exception is for lines starting with the special `domain:`, `username:`, and `password:` prefixes. When present, these prefixes are used when generating a new password or copying it to the clipboard. For example:

```
domain: www.example.com, www.example.co.uk
username: my-username
password: my-awesome-password

Any extra information down here.
```

Passwords for websites can be copied to the clipboard using `clip <URL>`, e.g. `clip https://www.example.com/login`.[^3] To prevent phishing attacks, the domain in the URL must be an exact match for one of the listed domains in the password entry. If you have multiple accounts on the same website, you can also pass an optional username to the end of `clip` to find the right entry.

New passwords for accounts can be generated using `gen <entry-name>`. By default, this will overwrite the `password:` line with a new 25 character password. The length and character set used for the password can be changed if desired, e.g. `gen example-entry 10 alnum`.

If you ever accidentally overwrote a password you didn't mean to, `log` will show you the history.

Other useful commands include:
- `list` will list entry names
- `show` will print an entry to the terminal
- `move` will rename an entry
- `del` will delete an entry and all its history

The complete set of available commands can be seen by running `help` in the REPL. Each command also has documentation that can be accessed, e.g. `help gen`.

Finally, the passphrase to the database file can be changed with the `napa pass` command. This will generate a new passphrase.

```
$ napa pass my-passwords.napa
Enter the passphrase:
Your new passphrase for this database file is: penalty-cement-legacy-surface-symphonic-confound
Remember this passphrase! It will not be displayed again.
```

### Security

The Napa database file is designed to be immune to specific attacks described in the [database document](https://codeberg.org/jwgarber/napa/src/branch/main/database.md). Once the database is unlocked, Napa makes a best-effort attempt to avoid exposing secret material to any external process. For example, all memory allocated during the program is zeroed on deallocation. Secret information can only be accessed by the rest of the system when copying to the clipboard (somewhat unavoidable, but the clipboard is automatically cleared after a 30 second timeout).

When possible, Napa should be compiled as a statically-linked binary to prevent [LD_PRELOAD attacks](https://attack.mitre.org/techniques/T1574/006/). Of course, this won't extend to the rest of your system (such as your terminal emulator), so it is recommended to use a sandbox (like [Flatpak](https://flatpak.org/) or [Snap](https://snapcraft.io/)) and a mandatory access control system such as [SELinux](https://github.com/SELinuxProject/selinux) or [AppArmor](https://gitlab.com/apparmor/apparmor).

Commits and tags that I author are signed with my [SSH key](https://www.jwgarber.ca/ssh/). After configuring the key, they can be verified using `git verify-commit` and `git verify-tag`. Commits prior to 2023-07-01 were signed with my [GPG key](https://www.jwgarber.ca/gpg/), but that key has expired and is no longer used.

### License

Napa is licensed under the MPL2.

### Credits

Napa took inspiration from several other cryptographic programs, including:

- [libsodium](https://doc.libsodium.org/)
- [minisign](https://github.com/jedisct1/minisign)
- [kickpass](https://github.com/kickpass/kickpass)
- [aegis](https://github.com/beemdevelopment/Aegis)
- [age](https://github.com/FiloSottile/age)

[^1]: Each release is built inside a SourceHut build environment, and the authenticity of the binary can be checked by calculating the `b2sum` and comparing it to the linked build page.

[^2]: This has roughly 77 bits of entropy.

[^3]: In your browser use Ctrl+L to copy the URL, Ctrl+Shift+V to paste it into Napa, and then Ctrl+V to paste the password.
