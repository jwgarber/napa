// SPDX-License-Identifier: MPL-2.0

use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::Write;
use std::os::unix::fs::OpenOptionsExt;
use std::path::Path;

use anyhow::{Context, Result, bail};

use crate::charset::CharSet;
use crate::header::Header;

#[derive(Debug, PartialEq, Eq)]
pub struct Database {
    pub header: [u8; Header::SIZE],
    pub ciphertext: Vec<u8>,
}

impl Database {
    pub fn read(database_path: &Path) -> Result<Database> {
        let content =
            std::fs::read(database_path).with_context(|| format!("Unable to read file {}", database_path.display()))?;

        if content.len() < Header::SIZE {
            bail!("Header length {} is less than required {}", content.len(), Header::SIZE);
        }

        // We just checked the size, so this unwrap won't fail.
        let header: [u8; Header::SIZE] = content[0..Header::SIZE].try_into().unwrap();
        let ciphertext = content[Header::SIZE..].to_vec();

        Ok(Database { header, ciphertext })
    }

    pub fn write_new(&self, database_path: &Path) -> Result<()> {
        self.write_database(database_path)?;
        sync_parent_dir(database_path)
    }

    // Overwrites the database file atomically. Based on https://lwn.net/Articles/457667/
    pub fn overwrite(&self, database_path: &Path) -> Result<()> {
        let random = CharSet::Alnum.random(16);

        // Renames are only atomic across the same filesystem, so create the
        // temporary file in the same directory as the database itself.
        let tempfile_extension = "tmp".to_owned() + &random;
        let tempfile_path = database_path.with_extension(tempfile_extension);

        self.write_database(&tempfile_path)?;

        fs::rename(&tempfile_path, database_path).with_context(|| {
            format!(
                "Unable to rename {} to {}",
                tempfile_path.display(),
                database_path.display()
            )
        })?;

        sync_parent_dir(database_path)
    }

    fn write_database(&self, path: &Path) -> Result<()> {
        let mut file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .mode(0o600) // u+rw
            .open(path)
            .with_context(|| format!("Unable to create file {}", path.display()))?;

        file.write_all(&self.header)
            .with_context(|| format!("Unable to write to file {}", path.display()))?;
        file.write_all(&self.ciphertext)
            .with_context(|| format!("Unable to write to file {}", path.display()))?;

        file.sync_all()
            .with_context(|| format!("Unable to synchronize file {}", path.display()))
    }
}

fn sync_parent_dir(database_path: &Path) -> Result<()> {
    let canon_path = database_path
        .canonicalize()
        .with_context(|| format!("Unable to find canonical path for {}", database_path.display()))?;
    let parent_path = canon_path
        .parent()
        .with_context(|| format!("Unable to find parent directory of {}", canon_path.display()))?;

    let parent_dir =
        File::open(parent_path).with_context(|| format!("Unable to open directory {}", parent_path.display()))?;
    parent_dir
        .sync_all()
        .with_context(|| format!("Unable to synchronize directory {}", parent_path.display()))
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_fs::NamedTempFile;

    fn gen_test_database() -> Database {
        Database {
            header: [0xAA; Header::SIZE],
            ciphertext: vec![0x55; 100],
        }
    }

    #[test]
    fn read_detects_too_short_header() {
        let test_file = NamedTempFile::new("test.napa").unwrap();

        std::fs::write(test_file.path(), vec![0xAA; 10]).unwrap();

        let result = Database::read(test_file.path());
        assert!(result.is_err());
    }

    #[test]
    fn read_write_roundtrips() {
        let test_file = NamedTempFile::new("test.napa").unwrap();

        let test_database = gen_test_database();
        test_database.write_new(test_file.path()).unwrap();

        let read_database = Database::read(test_file.path()).unwrap();

        assert_eq!(read_database, test_database);
    }

    #[test]
    fn write_new_fails_if_path_exists() {
        let test_file = NamedTempFile::new("test.napa").unwrap();
        let test_database = gen_test_database();

        test_database.write_new(test_file.path()).unwrap();

        let result = test_database.write_new(test_file.path());
        assert!(result.is_err());
    }

    #[test]
    fn can_overwrite_old_database() {
        let test_file = NamedTempFile::new("test.napa").unwrap();

        let mut test_database = gen_test_database();
        test_database.write_new(test_file.path()).unwrap();

        test_database.header[0] += 1;
        test_database.overwrite(test_file.path()).unwrap();

        let read_database = Database::read(test_file.path()).unwrap();

        assert_eq!(read_database, test_database);
    }
}
