// SPDX-License-Identifier: MPL-2.0

use std::pin::Pin;

use anyhow::{Result, anyhow};
use argon2::{Algorithm, Argon2, Params, Version};
use rand::rngs::OsRng;
use rand::{RngCore, TryRngCore};

use crate::crypto::chacha20::Key;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Iterations(pub u32);

impl Iterations {
    pub const SIZE: usize = 4;
}

impl Default for Iterations {
    fn default() -> Iterations {
        Iterations(4)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Memory(pub u32);

impl Memory {
    pub const SIZE: usize = 4;
}

impl Default for Memory {
    fn default() -> Memory {
        Memory(1048576)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Threads(pub u32);

impl Threads {
    pub const SIZE: usize = 4;
}

impl Default for Threads {
    fn default() -> Threads {
        Threads(1)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Salt(pub [u8; Salt::SIZE]);

impl Salt {
    pub const SIZE: usize = 16;

    pub fn generate() -> Salt {
        let mut buf = [0u8; Salt::SIZE];
        OsRng.unwrap_err().fill_bytes(&mut buf);
        Salt(buf)
    }
}

impl Default for Salt {
    fn default() -> Salt {
        Salt([0u8; Salt::SIZE])
    }
}

// Ensuring that secret data is cleared out of stack variables is essentially impossible,
// since the compiler moves the variables around in the stack and *does not call drop* when
// doing so. E.g. a stack-allocated key must be copied out of the stack frame in order to
// return it from a function, but that copy does not clear the old data in the frame. The
// solution is to pin the key on the heap, which ensures it won't move around and that all
// copies will be cleared using the zeroing allocator.
// See https://benma.github.io/2020/10/16/rust-zeroize-move.html

pub fn derive_key(
    password: &str,
    salt: &Salt,
    Iterations(iterations): Iterations,
    Memory(memory): Memory,
    Threads(threads): Threads,
) -> Result<Pin<Box<Key>>> {
    let mut key = Box::pin(Key([0u8; Key::SIZE]));

    let params = Params::new(memory, iterations, threads, None).map_err(|_| anyhow!("Invalid Argon2 parameters"))?;

    let argon2 = Argon2::new(Algorithm::Argon2id, Version::V0x13, params);

    argon2
        .hash_password_into(password.as_bytes(), &salt.0, &mut key.0)
        .map_err(|_| anyhow!("Unable to derive key. Do you have enough free memory?"))?;

    Ok(key)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vector() {
        let password = "Don't mention the war!";
        let salt = [
            0xF3, 0xCF, 0xB5, 0x45, 0x58, 0xBA, 0x4C, 0xEE, 0xD0, 0x6D, 0x6A, 0xF6, 0xDC, 0x06, 0x3D, 0xB2,
        ];
        let key = [
            0x22, 0x60, 0x23, 0xA2, 0x94, 0x6A, 0x5C, 0x51, 0xDD, 0xAB, 0x0B, 0x24, 0x51, 0x65, 0x6E, 0x8C, 0xEB, 0xA1,
            0xB7, 0x48, 0x25, 0xBF, 0x00, 0x0D, 0xCD, 0x20, 0x33, 0xDA, 0x99, 0x98, 0x28, 0xEE,
        ];

        let actual_key = derive_key(password, &Salt(salt), Iterations(2), Memory(65536), Threads(1)).unwrap();

        assert_eq!(key, actual_key.0);
    }
}
