// SPDX-License-Identifier: MPL-2.0

use anyhow::{Result, bail};

pub fn pad(buf: &mut Vec<u8>, blocksize: usize) -> Result<()> {
    if blocksize == 0 {
        bail!("Cannot pad with blocksize of 0");
    }

    buf.push(0x80);

    let bytes_over = buf.len() % blocksize;
    let padding = if bytes_over == 0 { 0 } else { blocksize - bytes_over };

    for _ in 0..padding {
        buf.push(0x00);
    }

    Ok(())
}

pub fn unpad(buf: &mut Vec<u8>) -> Result<()> {
    let unpadded_len = find_unpadded_length(buf)?;
    buf.truncate(unpadded_len);
    Ok(())
}

fn find_unpadded_length(buf: &[u8]) -> Result<usize> {
    for (i, byte) in buf.iter().enumerate().rev() {
        match byte {
            0x80 => return Ok(i),
            0x00 => continue,
            _ => bail!("Unpad error"),
        }
    }
    bail!("Unpad error");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn pad_adds_padding_bytes() {
        let mut buf = vec![0x1, 0x2];
        pad(&mut buf, 4).unwrap();
        assert_eq!(buf, vec![0x1, 0x2, 0x80, 0x0]);
    }

    #[test]
    fn pad_adds_only_terminator_byte() {
        let mut buf = vec![0x1, 0x2, 0x3];
        pad(&mut buf, 4).unwrap();
        assert_eq!(buf, vec![0x1, 0x2, 0x3, 0x80]);
    }

    #[test]
    fn pad_fails_if_blocksize_is_zero() {
        let mut buf = vec![0x1, 0x2, 0x3];
        let result = pad(&mut buf, 0);
        assert!(result.is_err());
    }

    #[test]
    fn unpad_removes_padding_bytes() {
        let mut buf = vec![0x1, 0x2, 0x80, 0x0];
        unpad(&mut buf).unwrap();
        assert_eq!(buf, vec![0x1, 0x2]);
    }

    #[test]
    fn unpad_removes_terminator_byte() {
        let mut buf = vec![0x1, 0x2, 0x3, 0x80];
        unpad(&mut buf).unwrap();
        assert_eq!(buf, vec![0x1, 0x2, 0x3]);
    }

    #[test]
    fn unpad_fails_if_invalid_byte_present() {
        let mut buf = vec![0x1, 0x2, 0x3, 0x8];
        let result = unpad(&mut buf);
        assert!(result.is_err());
    }

    #[test]
    fn unpad_fails_if_marker_byte_not_found() {
        let mut buf = vec![0x0, 0x0, 0x0];
        let result = unpad(&mut buf);
        assert!(result.is_err());
    }

    #[test]
    fn pad_unpad_roundtrips() {
        let orig_buf = vec![0x1, 0x2, 0x3, 0x4];
        let mut buf = orig_buf.clone();
        pad(&mut buf, 8).unwrap();
        unpad(&mut buf).unwrap();
        assert_eq!(buf, orig_buf);
    }
}
