// SPDX-License-Identifier: MPL-2.0

mod alloc;
mod charset;
mod cli;
mod crypto;
mod database;
mod entries;
mod header;
mod repl;
mod wordlist;

use anyhow::Result;
use clap::Parser;

use alloc::ZeroingAllocator;
use cli::Cli;

#[global_allocator]
static A: ZeroingAllocator = ZeroingAllocator;

fn main() -> Result<()> {
    secmem_proc::harden_process()?;
    Cli::parse().run()
}
