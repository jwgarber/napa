// SPDX-License-Identifier: MPL-2.0

use core::sync::atomic::Ordering;
use std::alloc::{GlobalAlloc, Layout, System};

pub struct ZeroingAllocator;

unsafe impl GlobalAlloc for ZeroingAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        unsafe { System.alloc(layout) }
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        unsafe {
            memzero(ptr, layout.size());
            #[cfg(not(test))]
            System.dealloc(ptr, layout);
        }
    }
}

// Perhaps use volatile_set_memory when stabilized
unsafe fn memzero(ptr: *mut u8, size: usize) {
    for i in 0..size {
        unsafe {
            core::ptr::write_volatile(ptr.add(i), 0);
        }
    }
    core::sync::atomic::compiler_fence(Ordering::SeqCst);
}

#[cfg(test)]
mod test {
    #[test]
    fn test_zero() {
        let mut a = Vec::with_capacity(2);

        a.push(0xde);
        a.push(0xad);
        let before_realloc: *const u8 = &a[0];

        a.push(0xbe);
        a.push(0xef);
        let after_realloc: *const u8 = &a[0];

        assert_eq!(&[0xde, 0xad, 0xbe, 0xef], &a[..]);

        assert_eq!(unsafe { core::slice::from_raw_parts(before_realloc, 2) }, &[0; 2]);
        drop(a);
        assert_eq!(unsafe { core::slice::from_raw_parts(after_realloc, 4) }, &[0; 4]);
    }
}
