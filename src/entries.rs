// SPDX-License-Identifier: MPL-2.0

use std::collections::BTreeMap;

use anyhow::Result;

use crate::crypto::chacha20;
use crate::crypto::chacha20::{Key, Nonce};
use crate::crypto::padding;
use crate::database::Database;
use crate::header::Header;

const BLOCKSIZE: usize = 1024;

pub type Entries = BTreeMap<String, Vec<String>>;

pub fn encrypt(header: &mut Header, entries: &Entries, key: &Key) -> Result<Database> {
    let mut plaintext = serialize_entries(entries);

    padding::pad(&mut plaintext, BLOCKSIZE)?;

    // Each encryption gets a new nonce
    header.nonce = Nonce::generate();
    let header_bytes = header.serialize();

    let ciphertext = chacha20::encrypt(&plaintext, &header_bytes, &header.nonce, key)?;

    let database = Database {
        header: header_bytes,
        ciphertext,
    };

    Ok(database)
}

pub fn decrypt(database: &Database, header: &Header, key: &Key) -> Result<Entries> {
    let mut plaintext = chacha20::decrypt(&database.ciphertext, &database.header, &header.nonce, key)?;

    padding::unpad(&mut plaintext)?;

    Ok(deserialize_entries(&plaintext))
}

fn serialize_entries(entries: &Entries) -> Vec<u8> {
    let mut bytes = Vec::new();
    for (name, history) in entries {
        for content in history {
            serialize_string(&mut bytes, name);
            serialize_string(&mut bytes, content);
        }
    }

    bytes
}

fn deserialize_entries(bytes: &[u8]) -> Entries {
    let mut entries = Entries::new();

    let length = bytes.len();
    let mut i: usize = 0;

    while i < length {
        let name = deserialize_string(bytes, &mut i);
        let content = deserialize_string(bytes, &mut i);
        let history = entries.entry(name).or_default();
        history.push(content)
    }

    assert_eq!(i, length);

    entries
}

fn serialize_string(bytes: &mut Vec<u8>, string: &str) {
    bytes.extend_from_slice(&u32::to_le_bytes(string.len().try_into().unwrap()));
    bytes.extend_from_slice(string.as_bytes());
}

fn deserialize_string(bytes: &[u8], i: &mut usize) -> String {
    let size: usize = u32::from_le_bytes(bytes[*i..*i + 4].try_into().unwrap())
        .try_into()
        .unwrap();
    *i += 4;
    let string = String::from_utf8(bytes[*i..*i + size].to_vec()).unwrap();
    *i += size;
    string
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn string_serialization_roundtrips() {
        let strings = ["", "abc", "Hello world!", "平仮名", "😀😁"];
        for string in strings.iter() {
            let mut bytes = Vec::new();
            let mut i = 0;
            serialize_string(&mut bytes, string);
            assert_eq!(*string, deserialize_string(&bytes, &mut i));
            assert_eq!(i, 4 + string.len());
        }
    }

    #[test]
    fn entries_serialization_roundtrips() {
        let mut entries = Entries::new();
        assert_eq!(entries, deserialize_entries(&serialize_entries(&entries)));

        entries.insert("".to_owned(), vec!["".to_owned(), "".to_owned()]);
        assert_eq!(entries, deserialize_entries(&serialize_entries(&entries)));

        entries.insert("Hello".to_owned(), vec!["World".to_owned(), "!".to_owned()]);
        assert_eq!(entries, deserialize_entries(&serialize_entries(&entries)));

        entries.insert("平仮名".to_owned(), vec!["😀😁".to_owned()]);
        assert_eq!(entries, deserialize_entries(&serialize_entries(&entries)));
    }

    #[test]
    fn encryption_roundtrips() {
        let key = Key([0xAA; Key::SIZE]);

        let mut header = Header::default();

        let mut entries = Entries::new();
        assert_eq!(
            entries,
            decrypt(&encrypt(&mut header, &entries, &key).unwrap(), &header, &key).unwrap()
        );

        entries.insert("".to_owned(), vec!["".to_owned(), "".to_owned()]);
        assert_eq!(
            entries,
            decrypt(&encrypt(&mut header, &entries, &key).unwrap(), &header, &key).unwrap()
        );

        entries.insert("Hello".to_owned(), vec!["World".to_owned(), "!".to_owned()]);
        assert_eq!(
            entries,
            decrypt(&encrypt(&mut header, &entries, &key).unwrap(), &header, &key).unwrap()
        );

        entries.insert("平仮名".to_owned(), vec!["😀😁".to_owned()]);
        assert_eq!(
            entries,
            decrypt(&encrypt(&mut header, &entries, &key).unwrap(), &header, &key).unwrap()
        );
    }

    #[test]
    fn database_header_corruption_is_detected() {
        let key = Key([0xAA; Key::SIZE]);
        let mut header = Header::default();

        let entries = Entries::new();
        let mut database = encrypt(&mut header, &entries, &key).unwrap();

        database.header[0] += 1;

        assert!(decrypt(&database, &header, &key).is_err());
    }

    #[test]
    fn database_ciphertext_corruption_is_detected() {
        let key = Key([0xAA; Key::SIZE]);
        let mut header = Header::default();

        let entries = Entries::new();
        let mut database = encrypt(&mut header, &entries, &key).unwrap();

        database.ciphertext[0] += 1;

        assert!(decrypt(&database, &header, &key).is_err());
    }
}
