// SPDX-License-Identifier: MPL-2.0

use std::io::BufRead;
use std::path::Path;

use anyhow::Result;

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::util;

pub fn del(name: &str, database_path: &Path, key: &Key) -> Result<()> {
    let (mut header, entries) = util::read_database(database_path, key)?;
    if let Some(new_entries) = del_impl(name, entries, &mut std::io::stdin().lock()) {
        util::write_database(database_path, &mut header, &new_entries, key)?;
    }
    Ok(())
}

fn del_impl(name: &str, mut entries: Entries, input: &mut impl BufRead) -> Option<Entries> {
    if entries.remove(name).is_some() {
        let prompt =
            format!("Warning: all history for {name} will be deleted. Are you sure you want to delete it? [y/N] ");
        let delete = util::confirm_prompt(&prompt, input);
        if delete {
            println!("Entries for {name} deleted.");
            Some(entries)
        } else {
            println!("Entries for {name} not deleted.");
            None
        }
    } else {
        println!("Error: entry for {name} does not exist.");
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const ENTRY: &str = "Entry";
    const VALUE: &str = "Value";

    #[test]
    fn entry_is_deleted_if_confirmed() {
        let entries = Entries::from([(ENTRY.to_owned(), vec![VALUE.to_owned()])]);

        let response = "y\n";
        let new_entries = del_impl(ENTRY, entries, &mut response.as_bytes()).unwrap();

        assert!(new_entries.is_empty());
    }

    #[test]
    fn entry_is_not_deleted_if_not_confirmed() {
        let entries = Entries::from([(ENTRY.to_owned(), vec![VALUE.to_owned()])]);

        let response = "n\n";
        let new_entries = del_impl(ENTRY, entries, &mut response.as_bytes());

        assert!(new_entries.is_none());
    }

    #[test]
    fn entries_unchanged_if_entry_name_does_not_exist() {
        let entries = Entries::from([(ENTRY.to_owned(), vec![VALUE.to_owned()])]);

        let new_entries = del_impl("Does Not Exist", entries, &mut "".as_bytes());

        assert!(new_entries.is_none());
    }
}
