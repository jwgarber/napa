// SPDX-License-Identifier: MPL-2.0

mod clear;
mod clip;
mod del;
mod do_move;
mod edit;
mod editor;
mod generate;
mod list;
mod log;
mod prefix;
mod show;
mod util;

use std::io::Write;
use std::path::Path;

use anyhow::Result;
use clap::Parser;
use rustyline::Editor;
use rustyline::config::Config;
use rustyline::error::ReadlineError;
use rustyline::history::MemHistory;

use crate::charset::CharSet;
use crate::crypto::chacha20::Key;

#[derive(Debug, Parser)]
#[clap(no_binary_name = true)]
pub enum Repl {
    #[clap(about = "List the entry names in the database")]
    List {
        #[clap(help = "Only list names that contain the given substring")]
        substr: Option<String>,
    },
    #[clap(about = "Edit the entry using the editor")]
    Edit { entry_name: String },
    #[clap(about = "Print the contents of the entry")]
    Show { entry_name: String },
    #[clap(about = "Print the history of the entry")]
    Log { entry_name: String },
    #[clap(about = "Delete the entry")]
    Del { entry_name: String },
    #[clap(about = "Rename the entry")]
    Move { old_name: String, new_name: String },
    #[clap(about = "Generate a new password for the entry")]
    Gen {
        #[clap(short, long, help = "Copy new password to clipboard")]
        clip: bool,
        entry_name: String,
        #[clap(default_value = "25", help = "Length of new password")]
        length: u32,
        #[clap(value_enum, default_value = "graph", help = "Charset to generate password from")]
        charset: CharSet,
    },
    #[clap(about = "Copy the entry password matching the domain and username to the clipboard")]
    Clip { url: String, username: Option<String> },
    #[clap(about = "Clear the screen")]
    Clear,
    #[clap(about = "Close the database")]
    Exit,
}

impl Repl {
    fn run(&self, database_path: &Path, key: &Key) -> Result<bool> {
        let mut exit = false;

        match self {
            Repl::List { substr } => list::list(substr, database_path, key)?,
            Repl::Log { entry_name } => log::log(entry_name, database_path, key)?,

            Repl::Edit { entry_name } => edit::edit(entry_name, database_path, key)?,
            Repl::Show { entry_name } => show::show(entry_name, database_path, key)?,
            Repl::Del { entry_name } => del::del(entry_name, database_path, key)?,
            Repl::Move { old_name, new_name } => do_move::do_move(old_name, new_name, database_path, key)?,

            Repl::Clear => clear::clear(),

            Repl::Gen {
                clip,
                entry_name,
                length,
                charset,
            } => generate::generate(*clip, entry_name, *length, *charset, database_path, key)?,
            Repl::Clip { url, username } => clip::clip(url, username, database_path, key)?,

            Repl::Exit => exit = true,
        }

        Ok(exit)
    }
}

pub fn repl(database_path: &Path, key: &Key) -> Result<()> {
    let config = Config::default();
    let history = MemHistory::with_config(config);
    let mut editor: Editor<(), MemHistory> = Editor::with_history(config, history)?;

    let mut exit = false;

    while !exit {
        let readline = editor.readline("> ");
        exit = match readline {
            Ok(line) => {
                editor.add_history_entry(&line)?;
                process_line(database_path, key, &line)?
            }
            Err(ReadlineError::Interrupted) => true, // CTRL-C
            Err(ReadlineError::Eof) => true,         // CTRL-D
            Err(err) => {
                println!("Error: {err:?}");
                true
            }
        };
        std::io::stdout().flush().unwrap();
    }

    Ok(())
}

fn process_line(database_path: &Path, key: &Key, line: &str) -> Result<bool> {
    let commands = line.split_whitespace().map(String::from).collect::<Vec<String>>();

    if commands.is_empty() {
        return Ok(false);
    }

    let result = Repl::try_parse_from(commands);

    match result {
        Ok(args) => args.run(database_path, key),
        Err(error) => {
            println!("{error}");
            Ok(false)
        }
    }
}
