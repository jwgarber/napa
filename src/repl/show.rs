// SPDX-License-Identifier: MPL-2.0

use std::path::Path;

use anyhow::Result;

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::util;

pub fn show(name: &str, database_path: &Path, key: &Key) -> Result<()> {
    let (_, entries) = util::read_database(database_path, key)?;
    show_impl(name, &entries);
    Ok(())
}

fn show_impl(name: &str, entries: &Entries) {
    match entries.get(name) {
        Some(history) => println!("{}", history.last().unwrap()),
        None => println!("Error: entry for {name} does not exist."),
    }
}
