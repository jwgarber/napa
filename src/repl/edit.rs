// SPDX-License-Identifier: MPL-2.0

use std::path::Path;

use anyhow::Result;

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::editor::Editor;
use crate::repl::util;

pub fn edit(name: &str, database_path: &Path, key: &Key) -> Result<()> {
    let (mut header, entries) = util::read_database(database_path, key)?;
    if let Some(new_entries) = edit_impl(name, entries, run_editor)? {
        util::write_database(database_path, &mut header, &new_entries, key)?;
    }
    Ok(())
}

fn edit_impl(name: &str, mut entries: Entries, edit: fn(&str, &str) -> Result<String>) -> Result<Option<Entries>> {
    if let Some(history) = entries.get_mut(name) {
        let content = history.last().unwrap();
        let new_content = edit(name, content)?;

        if new_content == *content {
            println!("Entry for {name} unchanged.");
            Ok(None)
        } else {
            history.push(new_content);
            println!("Edited entry for {name}.");
            Ok(Some(entries))
        }
    } else {
        let content = edit(name, "")?;
        entries.insert(name.to_owned(), vec![content]);
        println!("New entry inserted for {name}.");
        Ok(Some(entries))
    }
}

fn run_editor(name: &str, content: &str) -> Result<String> {
    let mut editor = Editor::new(name.to_string(), content.to_string())?;

    // None means the user quit without saving, so discard the changes
    let new_content = editor.run()?.unwrap_or(content.to_string());

    Ok(new_content.trim().to_owned())
}

#[cfg(test)]
mod tests {
    use super::*;

    const ENTRY: &str = "Entry";
    const OLD_VALUE: &str = "Old Value";
    const NEW_VALUE: &str = "New Value";

    fn old_value_stub(_name: &str, _content: &str) -> Result<String> {
        Ok(OLD_VALUE.to_owned())
    }

    fn new_value_stub(_name: &str, _content: &str) -> Result<String> {
        Ok(NEW_VALUE.to_owned())
    }

    #[test]
    fn edit_detects_if_content_unchanged() {
        let entries = Entries::from([(ENTRY.to_owned(), vec![OLD_VALUE.to_owned()])]);

        let new_entries = edit_impl(ENTRY, entries, old_value_stub).unwrap();

        assert!(new_entries.is_none());
    }

    #[test]
    fn edit_updates_content_if_entry_exists() {
        let entries = Entries::from([(ENTRY.to_owned(), vec![OLD_VALUE.to_owned()])]);

        let new_entries = edit_impl(ENTRY, entries, new_value_stub).unwrap().unwrap();

        let expected_entries = Entries::from([(ENTRY.to_owned(), vec![OLD_VALUE.to_owned(), NEW_VALUE.to_owned()])]);

        assert_eq!(new_entries, expected_entries);
    }

    #[test]
    fn edit_inserts_entry_if_does_not_exist() {
        let entries = Entries::new();

        let new_entries = edit_impl(ENTRY, entries, new_value_stub).unwrap().unwrap();

        let expected_entries = Entries::from([(ENTRY.to_owned(), vec![NEW_VALUE.to_owned()])]);

        assert_eq!(new_entries, expected_entries);
    }
}
