// SPDX-License-Identifier: MPL-2.0

use std::path::Path;
use std::time::Duration;

use anyhow::{Context, Result};
use url::Url;
use wl_clipboard_rs::copy;
use wl_clipboard_rs::copy::{ClipboardType, MimeType, Options, Seat, Source};

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::prefix;
use crate::repl::util;

pub const CLEAR_DURATION: Duration = Duration::from_secs(30);

pub fn clip(url: &str, username: &Option<String>, database_path: &Path, key: &Key) -> Result<()> {
    let (_, entries) = util::read_database(database_path, key)?;
    clip_impl(url, username, &entries, CLEAR_DURATION)
}

fn clip_impl(url: &str, username: &Option<String>, entries: &Entries, clear_duration: Duration) -> Result<()> {
    if let Ok(url) = Url::parse(url) {
        if let Some(domain) = url.domain() {
            let passwords = find_passwords(domain, username, entries);
            match &passwords[..] {
                [] => println!("Error: no password for {domain} found."),
                [password] => {
                    copy_to_clipboard(password.to_owned(), clear_duration)?;
                    println!("Password for {domain} copied to clipboard.");
                }
                _ => println!("Error: multiple passwords for {domain} found."),
            }
        } else {
            println!("Error: {url} does not contain a domain.");
        }
    } else {
        println!("Error: cannot parse {url} as a URL.");
    }
    Ok(())
}

fn find_passwords(domain: &str, username: &Option<String>, entries: &Entries) -> Vec<String> {
    if let Some(username) = username {
        find_passwords_with_username(domain, username, entries)
    } else {
        find_passwords_without_username(domain, entries)
    }
}

fn find_passwords_without_username(domain: &str, entries: &Entries) -> Vec<String> {
    let mut passwords = Vec::new();
    for history in entries.values() {
        let content = history.last().unwrap();
        if let Some(domains) = prefix::find_domain(content) {
            if domains.iter().any(|d| d == domain) {
                if let Some(password) = prefix::find_password(content) {
                    passwords.push(password);
                }
            }
        }
    }

    passwords
}

fn find_passwords_with_username(domain: &str, username: &str, entries: &Entries) -> Vec<String> {
    let mut passwords = Vec::new();
    for history in entries.values() {
        let content = history.last().unwrap();
        if let Some(domains) = prefix::find_domain(content) {
            if domains.iter().any(|d| d == domain) {
                if let Some(entry_username) = prefix::find_username(content) {
                    if username == entry_username {
                        if let Some(password) = prefix::find_password(content) {
                            passwords.push(password);
                        }
                    }
                }
            }
        }
    }

    passwords
}

pub fn copy_to_clipboard(password: String, clear_duration: Duration) -> Result<()> {
    std::thread::spawn(move || {
        std::thread::sleep(clear_duration);
        copy::clear(ClipboardType::Regular, Seat::All)
            .context("Unable to clear clipboard")
            .unwrap();
    });

    let mut opts = Options::new();
    opts.clipboard(ClipboardType::Regular);
    opts.copy(Source::Bytes(password.into_bytes().into()), MimeType::Text)
        .context("Unable to copy password to clipboard")
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::io::Read;

    use wl_clipboard_rs::paste;
    use wl_clipboard_rs::paste::{ClipboardType, Error, MimeType, Seat};

    fn paste() -> Result<String> {
        let (mut pipe, _) = paste::get_contents(ClipboardType::Regular, Seat::Unspecified, MimeType::Text)?;
        let mut contents = Vec::new();
        pipe.read_to_end(&mut contents)?;
        Ok(String::from_utf8(contents)?)
    }

    #[test]
    fn matching_passwords_are_returned() {
        let entries = Entries::from([
            (
                "ducksoup".to_owned(),
                vec![
                    "domain: www.ducksoup.com, www.ducksoup.co.uk\nusername: groucho\npassword: hugozhackenbush"
                        .to_owned(),
                ],
            ),
            (
                "ducksoup2".to_owned(),
                vec!["domain: www.ducksoup.com\nusername: groucho\npassword: rufustfirefly".to_owned()],
            ),
            (
                "ducksoup3".to_owned(),
                vec!["domain: www.ducksoup.com\nusername: chico\npassword: chicolini".to_owned()],
            ),
            (
                "buckprivates".to_owned(),
                vec!["domain: www.buckprivates.com\nusername: abbott\npassword: costello".to_owned()],
            ),
        ]);

        let username_passwords = find_passwords("www.ducksoup.com", &Some("groucho".to_owned()), &entries);

        assert_eq!(
            username_passwords,
            vec!["hugozhackenbush".to_owned(), "rufustfirefly".to_owned()]
        );

        let no_username_passwords = find_passwords("www.ducksoup.com", &None, &entries);

        assert_eq!(
            no_username_passwords,
            vec![
                "hugozhackenbush".to_owned(),
                "rufustfirefly".to_owned(),
                "chicolini".to_owned()
            ]
        );
    }

    #[test]
    #[ignore] // Doesn't work in CI environment since wayland isn't running
    fn password_of_entry_is_copied_and_cleared() {
        let domain = "www.example.com";
        let username = "test";
        let password = "hello!";
        let old_password = "world!";

        let name = "example".to_owned();
        let value = format!("domain: {domain}\nusername: {username}\npassword: {password}");
        let old_value = format!("domain: {domain}\nusername: {username}\npassword: {old_password}");

        let entries = Entries::from([(name, vec![old_value, value])]);

        let url = "https://www.example.com";

        let clear_duration = Duration::from_secs(1);
        let wait_duration = Duration::from_secs(2);

        clip_impl(url, &Some(username.to_owned()), &entries, clear_duration).unwrap();

        let pasted = paste().unwrap();
        assert_eq!(pasted, password);

        std::thread::sleep(wait_duration);

        let cleared_pasted = paste();
        let error = cleared_pasted.err().unwrap().downcast::<Error>().unwrap();
        assert!(matches!(error, Error::ClipboardEmpty));
    }
}
