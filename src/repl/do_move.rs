// SPDX-License-Identifier: MPL-2.0

use std::path::Path;

use anyhow::Result;

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::util;

pub fn do_move(source_name: &str, dest_name: &str, database_path: &Path, key: &Key) -> Result<()> {
    let (mut header, entries) = util::read_database(database_path, key)?;
    if let Some(new_entries) = do_move_impl(source_name, dest_name, entries) {
        util::write_database(database_path, &mut header, &new_entries, key)?;
    }
    Ok(())
}

fn do_move_impl(source_name: &str, dest_name: &str, mut entries: Entries) -> Option<Entries> {
    if let Some(content) = entries.remove(source_name) {
        if entries.insert(dest_name.to_owned(), content).is_some() {
            println!("Error: entry for {dest_name} already exists.");
            None
        } else {
            println!("Moved {source_name} to {dest_name}.");
            Some(entries)
        }
    } else {
        println!("Error: entry for {source_name} does not exist.");
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const SOURCE: &str = "Source";
    const SOURCE_VALUE: &str = "Source Value";

    const DEST: &str = "Dest";
    const DEST_VALUE: &str = "Dest Value";

    #[test]
    fn entries_unmodified_if_source_does_not_exist() {
        let entries = Entries::from([(DEST.to_owned(), vec![DEST_VALUE.to_owned()])]);

        let new_entries = do_move_impl(SOURCE, DEST, entries);

        assert!(new_entries.is_none());
    }

    #[test]
    fn entries_unmodified_if_dest_exists() {
        let entries = Entries::from([
            (SOURCE.to_owned(), vec![SOURCE_VALUE.to_owned()]),
            (DEST.to_owned(), vec![DEST_VALUE.to_owned()]),
        ]);

        let new_entries = do_move_impl(SOURCE, DEST, entries);

        assert!(new_entries.is_none());
    }

    #[test]
    fn source_moved_if_dest_does_not_exist() {
        let entries = Entries::from([(SOURCE.to_owned(), vec![SOURCE_VALUE.to_owned()])]);

        let new_entries = do_move_impl(SOURCE, DEST, entries).unwrap();

        let expected_entries = Entries::from([(DEST.to_owned(), vec![SOURCE_VALUE.to_owned()])]);

        assert_eq!(new_entries, expected_entries);
    }
}
