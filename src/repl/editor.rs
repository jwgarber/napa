// SPDX-License-Identifier: MPL-2.0

use std::io::Stdout;

use anyhow::{Context, Result};
use crossterm::event::{DisableMouseCapture, EnableMouseCapture};
use crossterm::terminal::{EnterAlternateScreen, LeaveAlternateScreen};
use ratatui::Terminal;
use ratatui::backend::CrosstermBackend;
use ratatui::layout::{Constraint, Direction, Layout};
use ratatui::style::{Color, Modifier, Style};
use ratatui::text::{Line, Span};
use ratatui::widgets::Paragraph;
use tui_textarea::{Input, Key, TextArea};

pub struct Editor<'a> {
    textarea: TextArea<'a>,
    term: Terminal<CrosstermBackend<Stdout>>,
    name: String,
}

impl Editor<'_> {
    pub fn new(name: String, content: String) -> Result<Self> {
        let mut textarea = TextArea::from(content.lines());
        textarea.set_line_number_style(Style::default().fg(Color::DarkGray));

        let mut stdout = std::io::stdout();
        crossterm::terminal::enable_raw_mode().context("Unable to enable raw mode")?;
        crossterm::execute!(stdout, EnterAlternateScreen, EnableMouseCapture).context("Unable to execute crossterm")?;
        let backend = CrosstermBackend::new(stdout);
        let term = Terminal::new(backend).context("Unable to create terminal")?;

        Ok(Self { textarea, term, name })
    }

    pub fn run(&mut self) -> Result<Option<String>> {
        let mut confirm_quit = false;
        let mut modified = false;

        loop {
            let layout = Layout::default()
                .direction(Direction::Vertical)
                .constraints([Constraint::Min(1), Constraint::Length(1), Constraint::Length(1)].as_ref());

            self.term.draw(|f| {
                let chunks = layout.split(f.area());

                f.render_widget(&self.textarea, chunks[0]);

                // Render status line
                let path = if modified {
                    format!(" {} [modified]", self.name)
                } else {
                    format!(" {}", self.name)
                };
                let (row, col) = self.textarea.cursor();
                let cursor = format!("({},{}) ", row + 1, col + 1);

                let status_chunks = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints(
                        [
                            Constraint::Min(1),
                            Constraint::Length(u16::try_from(cursor.len()).unwrap()),
                        ]
                        .as_ref(),
                    )
                    .split(chunks[1]);

                let status_style = Style::default().add_modifier(Modifier::REVERSED);
                f.render_widget(Paragraph::new(path).style(status_style), status_chunks[0]);
                f.render_widget(Paragraph::new(cursor).style(status_style), status_chunks[1]);

                // Render message at bottom
                let message = if confirm_quit {
                    Line::from(Span::raw("Are you sure you want to quit without saving? [y/N]"))
                } else {
                    Line::from(vec![
                        Span::raw("Press "),
                        Span::styled("^S", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to save and quit, "),
                        Span::styled("^Q", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to quit without saving, and the manual for other shortcuts"),
                    ])
                };

                f.render_widget(Paragraph::new(message), chunks[2]);
            })?;

            if confirm_quit {
                match crossterm::event::read().context("Unable to read event")?.into() {
                    Input {
                        key: Key::Char('y'), ..
                    } => return Ok(None),
                    _ => confirm_quit = false,
                }
            } else {
                match crossterm::event::read().context("Unable to read event")?.into() {
                    Input {
                        key: Key::Char('q'),
                        ctrl: true,
                        ..
                    } => {
                        if modified {
                            confirm_quit = true;
                        } else {
                            return Ok(None);
                        }
                    }
                    Input {
                        key: Key::Char('s'),
                        ctrl: true,
                        ..
                    } => {
                        let content = self.textarea.lines().to_vec().join("\n");
                        return Ok(Some(content));
                    }
                    input => {
                        modified = self.textarea.input(input);
                    }
                }
            }
        }
    }
}

impl Drop for Editor<'_> {
    fn drop(&mut self) {
        self.term.show_cursor().unwrap();
        crossterm::terminal::disable_raw_mode().unwrap();
        crossterm::execute!(self.term.backend_mut(), LeaveAlternateScreen, DisableMouseCapture).unwrap();
    }
}
