// SPDX-License-Identifier: MPL-2.0

use std::path::Path;

use anyhow::Result;

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::util;

pub fn list(substr: &Option<String>, database_path: &Path, key: &Key) -> Result<()> {
    let (_, entries) = util::read_database(database_path, key)?;
    list_impl(substr, &entries);
    Ok(())
}

fn list_impl(substr: &Option<String>, entries: &Entries) {
    let pattern = substr.clone().unwrap_or_default();
    for key in entries.keys() {
        if key.contains(&pattern) {
            println!("{key}");
        }
    }
}
