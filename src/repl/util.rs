// SPDX-License-Identifier: MPL-2.0

use std::io::BufRead;
use std::io::Write;
use std::path::Path;

use anyhow::{Context, Result};

use crate::crypto::chacha20::Key;
use crate::database::Database;
use crate::entries;
use crate::entries::Entries;
use crate::header::Header;

pub fn read_database(database_path: &Path, key: &Key) -> Result<(Header, Entries)> {
    let database = Database::read(database_path)?;
    let header = Header::deserialize(&database.header)?;
    let entries = entries::decrypt(&database, &header, key).context("Database has been corrupted!")?;
    Ok((header, entries))
}

pub fn write_database(database_path: &Path, header: &mut Header, entries: &Entries, key: &Key) -> Result<()> {
    let database = entries::encrypt(header, entries, key)?;
    database.overwrite(database_path)
}

pub fn confirm_prompt(prompt: &str, input: &mut impl BufRead) -> bool {
    print!("{prompt}");
    std::io::stdout().flush().unwrap();

    let mut response = String::new();
    input.read_line(&mut response).unwrap();

    matches!(response.trim(), "y" | "Y")
}

#[cfg(test)]
mod tests {
    use super::*;

    use assert_fs::NamedTempFile;

    use crate::entries::Entries;

    #[test]
    fn read_write_database_roundtrips() {
        let key = Key([0xAA; Key::SIZE]);
        let mut header = Header::default();

        let entries = Entries::from([
            ("Hello".to_owned(), vec!["World".to_owned(), "!".to_owned()]),
            ("平仮名".to_owned(), vec!["😀😁".to_owned()]),
        ]);

        let test_file = NamedTempFile::new("test.napa").unwrap();
        write_database(test_file.path(), &mut header, &entries, &key).unwrap();

        let (new_header, new_entries) = read_database(test_file.path(), &key).unwrap();

        assert_eq!(header, new_header);
        assert_eq!(entries, new_entries);
    }

    #[test]
    fn confirm_prompt_returns_true_for_yes() {
        let lower = "y\n";
        assert!(confirm_prompt("", &mut lower.as_bytes()));

        let upper = "Y\n";
        assert!(confirm_prompt("", &mut upper.as_bytes()));
    }

    #[test]
    fn confirm_prompt_returns_false_for_no() {
        let response = "n\n";
        assert!(!confirm_prompt("", &mut response.as_bytes()));

        let garbage = "aoeu\n";
        assert!(!confirm_prompt("", &mut garbage.as_bytes()));
    }
}
