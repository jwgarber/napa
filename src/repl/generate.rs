// SPDX-License-Identifier: MPL-2.0

use std::io::BufRead;
use std::path::Path;

use anyhow::Result;

use crate::charset::CharSet;
use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::clip::CLEAR_DURATION;
use crate::repl::clip::copy_to_clipboard;
use crate::repl::prefix;
use crate::repl::util;

pub fn generate(clip: bool, name: &str, length: u32, charset: CharSet, database_path: &Path, key: &Key) -> Result<()> {
    let (mut header, entries) = util::read_database(database_path, key)?;
    let new_password = charset.random(length);

    if let Some(new_entries) = gen_impl(name, &new_password, entries, &mut std::io::stdin().lock()) {
        util::write_database(database_path, &mut header, &new_entries, key)?;
        if clip {
            copy_to_clipboard(new_password, CLEAR_DURATION)?;
            println!("New password copied to clipboard.");
        }
    }
    Ok(())
}

fn gen_impl(name: &str, new_password: &str, mut entries: Entries, input: &mut impl BufRead) -> Option<Entries> {
    if let Some(history) = entries.get_mut(name) {
        let content = history.last().unwrap();
        if prefix::find_password(content).is_some() {
            let prompt = format!("Password for {name} already exists. Overwrite it? [y/N] ");
            let overwrite = util::confirm_prompt(&prompt, input);
            if overwrite {
                let new_content = prefix::replace_password(content, new_password);
                history.push(new_content);
                println!("Password for {name} overwritten.");
                Some(entries)
            } else {
                println!("Password for {name} not overwritten.");
                None
            }
        } else {
            // Else no password line found, so insert the new password at the start of the entry
            let new_content = format!("{} {}\n{}", prefix::PASSWORD, new_password, content);
            history.push(new_content.trim().to_owned());

            println!("New password generated for {name}.");
            Some(entries)
        }
    } else {
        // No existing entry, create new one
        let content = format!("{} {}", prefix::PASSWORD, new_password);
        entries.insert(name.to_owned(), vec![content]);
        println!("New password generated for {name}.");
        Some(entries)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gen_overwrites_password_if_confirmed() {
        let entry = "Entry";
        let new_password = "new_password";
        let old_value = "some stuff\npassword: old_password\nother stuff";
        let new_value = "some stuff\npassword: new_password\nother stuff";

        let entries = Entries::from([(entry.to_owned(), vec![old_value.to_owned()])]);

        let response = "y\n";
        let new_entries = gen_impl(entry, new_password, entries, &mut response.as_bytes()).unwrap();

        let expected_entries = Entries::from([(entry.to_owned(), vec![old_value.to_owned(), new_value.to_owned()])]);

        assert_eq!(new_entries, expected_entries);
    }

    #[test]
    fn gen_does_not_overwrite_password_if_not_confirmed() {
        let entry = "Entry";
        let new_password = "new_password";
        let old_value = "some stuff\npassword: old_password\nother stuff";

        let entries = Entries::from([(entry.to_owned(), vec![old_value.to_owned()])]);

        let response = "n\n";
        let new_entries = gen_impl(entry, new_password, entries, &mut response.as_bytes());

        assert!(new_entries.is_none());
    }

    #[test]
    fn gen_adds_password_to_start_if_no_existing_password_exists() {
        let entry = "Entry";
        let new_password = "secret!";
        let old_value = "some stuff";
        let new_value = "password: secret!\nsome stuff";

        let entries = Entries::from([(entry.to_owned(), vec![old_value.to_owned()])]);

        let new_entries = gen_impl(entry, new_password, entries, &mut "".as_bytes()).unwrap();

        let expected_entries = Entries::from([(entry.to_owned(), vec![old_value.to_owned(), new_value.to_owned()])]);

        assert_eq!(new_entries, expected_entries);
    }

    #[test]
    fn gen_creates_new_entry_if_does_not_exist() {
        let entry = "Entry";
        let new_password = "secret!";
        let new_value = "password: secret!";

        let entries = Entries::new();

        let new_entries = gen_impl(entry, new_password, entries, &mut "".as_bytes()).unwrap();

        let expected_entries = Entries::from([(entry.to_owned(), vec![new_value.to_owned()])]);

        assert_eq!(new_entries, expected_entries);
    }
}
