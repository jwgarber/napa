// SPDX-License-Identifier: MPL-2.0

use termion::{clear, cursor};

pub fn clear() {
    print!("{}{}", clear::All, cursor::Goto(1, 1));
}
