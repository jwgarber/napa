// SPDX-License-Identifier: MPL-2.0

use std::fmt::Write;
use std::path::Path;

use anyhow::Result;
use minus::{ExitStrategy, Pager};

use crate::crypto::chacha20::Key;
use crate::entries::Entries;
use crate::repl::util;

pub fn log(name: &str, database_path: &Path, key: &Key) -> Result<()> {
    let (_, entries) = util::read_database(database_path, key)?;
    log_impl(name, &entries)
}

fn log_impl(name: &str, entries: &Entries) -> Result<()> {
    if let Some(history) = entries.get(name) {
        let mut pager = Pager::new();
        pager.set_exit_strategy(ExitStrategy::PagerQuit)?;
        pager.set_prompt(name)?;

        let columns = termion::terminal_size()?.0;
        let bar = "─".repeat(columns.into());

        for content in history.iter().rev() {
            writeln!(pager, "{bar}\n{content}")?;
        }

        minus::page_all(pager)?;
    } else {
        println!("Error: entry for {name} does not exist.");
    }

    Ok(())
}
