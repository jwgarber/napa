// SPDX-License-Identifier: MPL-2.0

pub const DOMAIN: &str = "domain:";
pub const USERNAME: &str = "username:";
pub const PASSWORD: &str = "password:";

pub fn find_domain(content: &str) -> Option<Vec<String>> {
    find(DOMAIN, content).map(|domains| domains.split(',').map(|d| d.trim().to_owned()).collect())
}

pub fn find_username(content: &str) -> Option<String> {
    find(USERNAME, content)
}

pub fn find_password(content: &str) -> Option<String> {
    find(PASSWORD, content)
}

fn find(prefix: &str, content: &str) -> Option<String> {
    for line in content.lines() {
        if let Some(stripped) = line.strip_prefix(prefix) {
            let value = stripped.trim().to_owned();
            return Some(value);
        }
    }

    None
}

pub fn replace_password(content: &str, password: &str) -> String {
    replace(PASSWORD, password, content)
}

fn replace(prefix: &str, new_value: &str, content: &str) -> String {
    let mut new_content = String::new();

    let mut found_prefix = false;
    for line in content.lines() {
        let new_line = if line.starts_with(prefix) && !found_prefix {
            found_prefix = true;
            format!("{prefix} {new_value}")
        } else {
            line.to_owned()
        };
        new_content.push_str(&new_line);
        new_content.push('\n');
    }

    new_content.trim().to_owned()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_domain_returns_list_of_domains() {
        let content = "domain: example.com, example.ca";
        assert_eq!(find_domain(content).unwrap(), vec!["example.com", "example.ca"]);
    }

    #[test]
    fn find_username_returns_username() {
        let content = "username: my-username";
        assert_eq!(find_username(content).unwrap(), "my-username");
    }

    #[test]
    fn find_password_returns_none_if_no_password() {
        let content = "no password here";
        assert!(find_password(content).is_none());
    }

    #[test]
    fn find_password_returns_first_password() {
        let content = "first line\npassword: first password  \npassword: second password";
        assert_eq!(find_password(content), Some("first password".to_owned()));
    }

    #[test]
    fn replace_password_does_nothing_if_no_password_found() {
        let content = "no password here";
        assert_eq!(replace_password(content, "new password"), content);
    }

    #[test]
    fn replace_password_replaces_first_password() {
        let content = "first line\npassword: first password   \npassword: second password";
        assert_eq!(
            replace_password(content, "new password"),
            "first line\npassword: new password\npassword: second password"
        );
    }
}
