// SPDX-License-Identifier: MPL-2.0

use clap::ValueEnum;
use rand::TryRngCore;
use rand::rngs::OsRng;
use rand::seq::IndexedRandom;

const DIGIT: &str = "0123456789";
const LOWER: &str = "abcdefghijklmnopqrstuvwxyz";
const UPPER: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const PUNCT: &str = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

// Hmm, how to reduce this duplication?
const ALPHA: &str = concat!("abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

const ALNUM: &str = concat!("abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789");

const GRAPH: &str = concat!(
    "abcdefghijklmnopqrstuvwxyz",
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "0123456789",
    "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~",
);

#[derive(Clone, Copy, Debug, ValueEnum)]
pub enum CharSet {
    Digit,
    Lower,
    Upper,
    Punct,
    Alpha,
    Alnum,
    Graph,
}

impl CharSet {
    fn chars(self) -> Vec<char> {
        match self {
            CharSet::Digit => DIGIT,
            CharSet::Lower => LOWER,
            CharSet::Upper => UPPER,
            CharSet::Punct => PUNCT,
            CharSet::Alpha => ALPHA,
            CharSet::Alnum => ALNUM,
            CharSet::Graph => GRAPH,
        }
        .chars()
        .collect()
    }

    pub fn random(self, length: u32) -> String {
        let chars = self.chars();
        (0..length)
            .map(|_| chars.choose(&mut OsRng.unwrap_err()).unwrap())
            .collect::<String>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn random_string_length_matches_input_length() {
        let string = CharSet::Graph.random(20);
        assert_eq!(string.len(), 20);
    }

    #[test]
    fn random_string_characters_are_taken_from_charset() {
        let charset = CharSet::Graph;
        let string = charset.random(20);
        assert!(string.chars().all(|c| charset.chars().contains(&c)))
    }
}
