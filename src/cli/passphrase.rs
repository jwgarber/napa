// SPDX-License-Identifier: MPL-2.0

use std::io::Write;

use termion::input::TermRead;

use crate::crypto::argon2::Salt;
use crate::header::Header;
use crate::wordlist::WordList;

const NUM_WORDS: u32 = 6;

pub fn new_passphrase(header: &mut Header) -> String {
    let passphrase = (0..NUM_WORDS)
        .map(|_| WordList::EffLarge.random())
        .collect::<Vec<_>>()
        .join("-");
    println!("The new passphrase for this database file is: {passphrase}");
    println!("Remember this passphrase! It will not be displayed again.");

    // Every new passphrase gets a new salt
    header.salt = Salt::generate();

    passphrase
}

pub fn read_passphrase(prompt: &str) -> Option<String> {
    let mut stdout = std::io::stdout().lock();
    let mut stdin = std::io::stdin().lock();

    stdout.write_all(prompt.as_bytes()).unwrap();
    stdout.flush().unwrap();

    let passphrase = stdin.read_passwd(&mut stdout).unwrap();

    stdout.write_all(b"\n").unwrap();
    stdout.flush().unwrap();

    passphrase
}
