// SPDX-License-Identifier: MPL-2.0

use std::path::Path;

use anyhow::{Context, Result};

use crate::cli::key;
use crate::cli::passphrase;
use crate::database::Database;
use crate::entries;
use crate::header::Header;

pub fn pass(database_path: &Path) -> Result<()> {
    let database = Database::read(database_path)?;
    let mut header = Header::deserialize(&database.header)?;

    if let Some(passphrase) = passphrase::read_passphrase("Enter the passphrase: ") {
        let key = key::derive_key(&passphrase, &header)?;

        // We decrypt the database here to ensure the right passphrase was entered.
        let entries = entries::decrypt(&database, &header, &key).context("Did you enter the wrong passphrase?")?;

        let new_passphrase = passphrase::new_passphrase(&mut header);
        let new_key = key::derive_key(&new_passphrase, &header)?;

        let new_database = entries::encrypt(&mut header, &entries, &new_key)?;
        new_database.overwrite(database_path)
    } else {
        Ok(())
    }
}
