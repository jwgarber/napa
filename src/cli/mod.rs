// SPDX-License-Identifier: MPL-2.0

mod init;
mod key;
mod open;
mod pass;
mod passphrase;

use std::path::PathBuf;

use anyhow::Result;
use clap::Parser;

#[derive(Debug, Parser)]
#[clap(version, about)]
pub enum Cli {
    #[clap(about = "Initialize a new database")]
    Init {
        #[clap(value_parser)]
        database_path: PathBuf,
    },
    #[clap(about = "Open an existing database")]
    Open {
        #[clap(value_parser)]
        database_path: PathBuf,
    },
    #[clap(about = "Change the passphrase of an existing database")]
    Pass {
        #[clap(value_parser)]
        database_path: PathBuf,
    },
}

impl Cli {
    pub fn run(&self) -> Result<()> {
        match self {
            Cli::Init { database_path } => init::init(database_path),
            Cli::Open { database_path } => open::open(database_path),
            Cli::Pass { database_path } => pass::pass(database_path),
        }
    }
}
