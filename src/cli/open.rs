// SPDX-License-Identifier: MPL-2.0

use std::path::Path;
use std::pin::Pin;

use anyhow::{Context, Result};

use crate::cli::key;
use crate::cli::passphrase;
use crate::crypto::chacha20::Key;
use crate::database::Database;
use crate::entries;
use crate::header::Header;
use crate::repl;

pub fn open(database_path: &Path) -> Result<()> {
    if let Some(key) = read_key(database_path)? {
        repl::repl(database_path, &key)
    } else {
        Ok(())
    }
}

fn read_key(database_path: &Path) -> Result<Option<Pin<Box<Key>>>> {
    let database = Database::read(database_path)?;
    let header = Header::deserialize(&database.header)?;

    if let Some(passphrase) = passphrase::read_passphrase("Enter the passphrase: ") {
        let key = key::derive_key(&passphrase, &header)?;

        // Attempt to decrypt the database now. If it fails, then you (probably) entered the wrong passphrase.
        // Once the key is loaded into memory, further decryption failures indicate the database has been
        // corrupted or tampered with.
        entries::decrypt(&database, &header, &key).context("Did you enter the wrong passphrase?")?;

        Ok(Some(key))
    } else {
        Ok(None)
    }
}
