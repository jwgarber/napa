// SPDX-License-Identifier: MPL-2.0

use std::path::Path;

use anyhow::Result;

use crate::cli::key;
use crate::cli::passphrase;
use crate::entries;
use crate::entries::Entries;
use crate::header::Header;

pub fn init(database_path: &Path) -> Result<()> {
    let mut header = Header::default();

    let passphrase = passphrase::new_passphrase(&mut header);

    let key = key::derive_key(&passphrase, &header)?;

    // No entries to write when creating a new database
    let entries = Entries::new();
    let database = entries::encrypt(&mut header, &entries, &key)?;

    database.write_new(database_path)
}
