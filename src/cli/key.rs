// SPDX-License-Identifier: MPL-2.0

use std::io::Write;
use std::pin::Pin;

use anyhow::Result;

use crate::crypto::argon2;
use crate::crypto::chacha20::Key;
use crate::header::Header;

pub fn derive_key(passphrase: &str, header: &Header) -> Result<Pin<Box<Key>>> {
    println!("Deriving key...");
    std::io::stdout().flush().unwrap();
    argon2::derive_key(
        passphrase,
        &header.salt,
        header.iterations,
        header.memory,
        header.threads,
    )
}
